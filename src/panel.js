import React from "react"
import Voideship from "./voivodeship"

class Panel extends React.Component {
    state = {
        activeVoivodeship: "Mazowieckie",
        voivodeships: [
            "Dolnośląskie",
            "Kujawsko-pomorskie",
            "Lubelskie",
            "Lubuskie",
            "Łódzkie",
            "Małopolskie" ,
            "Mazowieckie",
            "Opolskie",
            "Podkarpackie",
            "Podlaskie",
            "Pomorskie",
            "Śląskie",
            "Świętokrzyskie",
            "Warmińsko-mazurskie",
            "Wielkopolskie",
            "Zachodniopomorskie"  
        ]
    }
    
    updateActiveVoivodeship(e) {
        this.setState({
            activeVoivodeship: e.target.innerText
        })
        this.props.onUpdateActiveVoivodeship(e.target.innerText)
    }

    render(){
        return (
            <div className="container-fluid">
                <div class="row">
                    <h3 class="text-center">Województwa</h3>
                    <div class="container-flex  border-bottom mt-2 mb-4"></div>
                    <div class="col-4 d-flex flex-wrap">
                        {this.state.voivodeships.map(item =>
                            this.state.activeVoivodeship === item ?
                                <button class="btn btn-primary p-2 mx-2 mt-2" onClick={this.updateActiveVoivodeship.bind(this)}>
                                    {item} 
                                </button>
                                :
                                <button class="btn btn-secondary p-2 mx-2 mt-2" onClick={this.updateActiveVoivodeship.bind(this)}>
                                    {item} 
                                </button>
                        )}        
                    </div>
                    <div class="col-8 mt-4">
                        <Voideship name={this.state.activeVoivodeship}/>
                    </div>
                </div>
            </div>
        )
    }
}



export default Panel