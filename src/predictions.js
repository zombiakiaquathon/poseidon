import React from "react"
import Prediction from "./prediction"
import { faCloudShowersHeavy, faGlassWhiskey, faTint, faSun, faTintSlash } from '@fortawesome/free-solid-svg-icons'



const messages = [
    {
        header: "Brakuje wody",
        message: "Wykryto X obszarów gdzie będzie możliwy niedomiar wody!",
        icon: faTintSlash
    },
    {
        header: "Susza",
        message: "W preciągu X dni może nastąpić susza",
        icon: faSun
    },
    {
        header: "Nadmiar wody",
        message: "W tym województwie ilość wody bardzo wzrosło i zostanie osiągniety nadmiar ",
        icon: faTint
    },
    {
        header: "Zużycie wody",
        message: "Wykryto X obszarów gdzie zużycie wody wzrosło i może przekroczyć normę!",
        icon: faGlassWhiskey
    },
    {
        header: "Obfite opady",
        message: "W tym województwie prognozowane są bardzo obfite opady. Ilość wody wzrośnie o X",
        icon: faCloudShowersHeavy
    },
]


function generatePredictions() {
    let n = parseInt(Math.random() * (5 - 1) + 1)
    let predictions = []

    for (let i=0; i<n; ++i) {
        predictions.push(messages[parseInt(Math.random() * messages.length)])
    }

    return predictions
}


class Predictions extends React.Component {
    state =  {
        name:   "lol",
        predictions:  [

        ]
    }

    static getDerivedStateFromProps(props, state) {
        return {
            name: props.name,
            predictions: generatePredictions()
        }
    }

    render() {
        return (
            <div class="container-fluid h-100 mt-5">
                <div class="row d-flex align-items-center">
                    <div class="position-relative">
                        <h3 class="mx-2">Prognozy</h3>
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                            {this.state.predictions.length}+
                        </span>
                    </div>
                </div>
                <div  class="row border-bottom mt-2"></div>
                <div class="d-flex mt-5 mb-5">
                    {this.state.predictions.map(prediction =>
                        <Prediction header={prediction.header} message={prediction.message} icon={prediction.icon} />
                    )

                    }
                </div>
            </div>
         
        )
    }
}

export default Predictions