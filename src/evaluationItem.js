import React from "react"


class EvaluationItemDetail extends React.Component {
    state = {
        message: "l"
    }

    static getDerivedStateFromProps(props, state) {
        return  {
            message: props.message
        }    
    }

    render() {
        return (
            <span class="badge bg-success">{this.state.message}</span>
        )
    }
}

export default EvaluationItemDetail