import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSmileBeam } from '@fortawesome/free-solid-svg-icons'
import EvaluationItem from "./evaluationItems"



class Evaluation extends React.Component {
    state = {
        bads: [],
        goods: [],
        rate: "Dobry"
    }

    render() {
        return (
            <div class="row">
                <h3>Ocena</h3>
                <div class="d-flex border-bottom mb-5"></div>
                <div class="row">
                    <div class="col-4">
                        <EvaluationItem />
                    </div>

                    <div class="col-4">
                            <div class="d-flex justify-content-center">
                                <FontAwesomeIcon icon={faSmileBeam} size="6x" />
                            </div>
                            <p class="text-center mt-2"><b>{this.state.rate}</b></p>
                  
                    </div>

                    <div class="col-4">
                    <EvaluationItem />

                    </div>
                </div>
            </div>
        )
    }
}




export default Evaluation;