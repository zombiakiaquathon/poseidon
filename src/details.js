import React from "react"
import { Line } from 'react-chartjs-2';


function randomData() {
    var data = []

    for (let i=0; i<12;  ++i) {
        data.push(Math.random()  * 50)
    }

    return data
}


class Details extends React.Component  {
    name = this.props.name
    months  = [
        "Styczeń",
        "Luty",
        "Marzec",
        "Kwiecień",
        "Maj",
        "Czerwiec",
        "Lipiec",
        "Sierpień",
        "Wrzesień",
        "Październik",
        "Listopad",
        "Grudzień"
    ]

    static getDerivedStateFromProps(props, state) {
        return {
            name: props.name
        }
    }

    state = {
        data: {
            labels: this.months,
            datasets: [{
                type: "line",
                label: "Zużycie wody",
                data: randomData(),
                fill: false,
                borderColor: 'rgb(0, 153, 204)',
                tension: 0.1
            },
            {
                type:  "bar",
                label: "Produkowane ścieki",
                data: randomData(),
                fill: false,
                backgroundColor: 'rgb(204,34,0)',
                tension: 0.1
            },
            {
                type:  "bar",
                label: "Ścieki oczyszczone",
                data: randomData(),
                fill: false,
                backgroundColor: 'rgb(0, 204, 102)',
                tension: 0.1
            }]
        }
    }

 
    render() {
        return (
            <div class="container-fluid ">
                <div class="row mt-5">
                    <h3>Szczegóły dla <b>{this.state.name}</b></h3>
                    <div class="d-flex border-bottom"></div>
                </div>
                <div class="row mt-5">
                    <Line data={this.state.data}  />
                </div>
            </div>
        )
    }
}

export default Details