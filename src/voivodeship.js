import React from "react"
import VoidshipSummaryItem from "./voivodeshipSummaryItem"
import { Doughnut } from 'react-chartjs-2';
import { faUserFriends, faHandHoldingWater } from '@fortawesome/free-solid-svg-icons'



function randomData() {
    let data = []

    for (let i=0; i<4; ++i) {
        data.push(Math.random() *  (100  - 5)  + 5)
    }

    return data
}


class Voideship extends React.Component {
    state = {
        name: this.props.name,
        people: 560000,
        water: 15,
        data: {
            labels: ["Klasa 1", "Klasa 2", "Klasa 3", "Wody pozaklasowe"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.2)",
                    data: randomData(),
                    backgroundColor: [
                        'rgb(0, 51, 204)',
                        'rgb(51, 204, 51)',
                        'rgb(255, 255, 102)',
                        'rgb(255, 51, 0)',      
                    ],
                }
            ]
        }
    }

    static getDerivedStateFromProps(props, state) {
        return {
            name: props.name,
            people: parseInt(Math.random() *  (50 - 5) + 5) * 10000,
            water: parseInt(Math.random() * (15 - 1) + 1) * 100,
            data: {
                labels: ["Klasa 1", "Klasa 2", "Klasa 3", "Wody pozaklasowe"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.2)",
                        data: randomData(),
                        backgroundColor: [
                            'rgb(0, 51, 204)',
                            'rgb(51, 204, 51)',
                            'rgb(255, 255, 102)',
                            'rgb(255, 51, 0)',      
                        ],
                    }
                ]
            }
        }
      }

    render() {
        return  (
            <div class="row">
                <h2 class="text-center"><b>{this.state.name}</b></h2>
                <div class="row p-2 mx-2 mt-3">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="col-4">
                            <VoidshipSummaryItem header={this.state.people} text={"mieszkańców"} icon={faUserFriends}/>
                        </div>
                        <div class="col-4">
                            <Doughnut data={this.state.data} />
                        </div>
                        <div class="col-4">
                        <   VoidshipSummaryItem header={this.state.water} text={"Litrów wody na mieszkańca"} icon={faHandHoldingWater}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
} 


export default Voideship