import React from "react"
import Logo from "./logo"


class Header extends React.Component {
    render() {
        return (
            <div class="row p-5">
                <h1 class="text-center">POSEIDON</h1>
                <Logo />
            </div>
        )
    }
}

export default Header;
