import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'


class VoideshipSummaryItem extends React.Component {
    state = {
        header: "Test",
        icon: faCoffee,
        text: "Number"
    }

    static getDerivedStateFromProps(props, state) {
        return {
            header: props.header,
            text: props.text,
            icon: props.icon
        }
      }

    render() {
        return (
            <div class="row mx-2 p-5 justify-content-center" >
                <h3 class="card-title text-center">{this.state.header}</h3>
                <FontAwesomeIcon  icon={this.state.icon} size={"6x"}/>
                <p class="card-text text-center"> {this.state.text} </p>
            </div>
        )
    }
}

export default VoideshipSummaryItem