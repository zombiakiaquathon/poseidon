import React from "react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Prediction extends React.Component {
    state = {
        header: "lol",
        message: "???"
    }

    static getDerivedStateFromProps(props, state) {
        return {
            header: props.header,
            message: props.message,
            icon: props.icon
        }
    }

    render() {
        return (
            <div class="card text-white bg-primary mb-3 mx-3 h-100" style={{width: 350}}>
                  <div class="card-header">
                        <FontAwesomeIcon icon={this.state.icon}/>
                        <b class="mx-2">{this.state.header}</b>
                  </div>
                <div class="card-body">
                    <p class="card-text">{this.state.message}</p>
                </div>
                <div class="card-footer">
                    <a href="#" class="btn btn-warning">Analizuj</a>
                </div>
           
            </div>
        )
    } 
}

export default Prediction