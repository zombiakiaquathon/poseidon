import Header from "./header"
import Panel from  "./panel"
import './App.css';
import React from "react";
import { MapContainer, TileLayer, Marker, Popup, GeoJSON, useMap } from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import Details from "./details"
import Predictions from  "./predictions"

function MapReloader(props) {
  const map = useMap()
  map.setView(props.center, props.zoom)
  return null;
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        load_voivodeship: false,
        states: null,
        voivodeship: null,
        voivodeship_active: null,
        voivodeship_active_position: [52.09, 19.15],
        voivodeships_positions: {
          "Dolnośląskie": [51.06, 17.01],
          "Kujawsko-pomorskie": [54.20, 18.38],
          "Lubelskie": [51.14, 22.34],
          "Lubuskie": [52.43, 15.14],
          "Łódzkie": [51.46, 19.27],
          "Małopolskie": [50.03, 19.56],
          "Mazowieckie": [52.13, 21.00],
          "Opolskie": [50.39, 17.55],
          "Podkarpackie": [50.02, 22.00],
          "Podlaskie": [53.08, 23.08],
          "Pomorskie": [54.20, 18.38],
          "Śląskie": [50.15, 19.01],
          "Świętokrzyskie": [50.53, 20.37],
          "Warmińsko-mazurskie": [53.47, 20.30],
          "Wielkopolskie": [52.24, 16.56],
          "Zachodniopomorskie": [53.26, 14.32]  
        },
        zoom: 6,
        scroll_wheel_zoom: false
    }
    this.updateActiveVoivodeshipParent = this.updateActiveVoivodeshipParent.bind(this)
  }

  componentDidMount() {
    fetch("https://raw.githubusercontent.com/ppatrzyk/polska-geojson/master/wojewodztwa/wojewodztwa-medium.geojson").then((response) => {
        return response.json();
    }).then((data) => {
      this.setState({ 
        voivodeship: data
      })
    })
    fetch("https://raw.githubusercontent.com/ppatrzyk/polska-geojson/master/powiaty/powiaty-medium.geojson").then((response) => {
        return response.json();
    }).then((data) => {
        this.setState({ 
          states: data
        })
    })
  }

  updateActiveVoivodeshipParent(voivodeship_active) {
    this.setState({
      voivodeship_active: voivodeship_active
    }, () => this.targetVoivodeshipOnMap(this.state.voivodeship_active))
  }
  targetVoivodeshipOnMap(voivodeship) {
    this.setState({
      voivodeship_active_position: this.state.voivodeships_positions[voivodeship],
      zoom: 8
    })
  }

  renderGeoJson(voivodeship, voivodeship_active) {
    let voivodeship_style = { color: 'green' };
    if (voivodeship !== null && voivodeship_active == null) {
      const voivodeship_json = this.state.voivodeship;
      return <GeoJSON data={voivodeship_json} style={voivodeship_style}/>
    }
    let state_style = { color: 'red' };
    if (voivodeship_active !== null) {
      const state_json = this.state.states;
      return <GeoJSON data={state_json} style={state_style}/>
    }
  }

  renderMap() {
    console.log(this.state.voivodeship_active)
    console.log(this.state.zoom)
    console.log(this.state.voivodeship_active_position)
    return (
      <MapContainer center={this.state.voivodeship_active_position} zoom={this.state.zoom} scrollWheelZoom={this.state.scroll_wheel_zoom}>
        <MapReloader center={this.state.voivodeship_active_position} zoom={this.state.zoom}/>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {this.renderGeoJson(this.state.voivodeship, this.state.voivodeship_active)}
      </MapContainer>
    );
  }

  render() {
    return (
      <div class="container">
        <div class="row">
          <Header />
        </div>
        <div class="row">
          <Panel onUpdateActiveVoivodeship={this.updateActiveVoivodeshipParent}/>
        </div>
        <div class="row">
          <div class="row">
            <h2 className="mt-3">Mapa</h2>
            <div class="row border-bottom mb-4"></div>
          </div>
          <div class="row">
            <div class="col-12 pb-3">
              {this.renderMap()}
            </div>            
          </div>
          <div class="row">
            <div class="col-6 ">
                <Predictions name={this.state.voivodeship_active} />
            </div>
            <div class="col-6 d-flex flex-wrap">
                <Details name={this.state.voivodeship_active} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
