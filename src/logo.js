import React  from "react"


class Logo extends React.Component {
    render() {
        return(
            <div class="d-flex mt-2 justify-content-center">
            <div style={{backgroundColor: "rgba(0, 0, 204, 0.5)", borderRadius: "50%"}}>
                <img  src="https://i.postimg.cc/J4C3gRp2/poseidon.png" style={{width: 250, height: 250,  transform:"scale(0.70)"}}/>
            </div>
        </div>
        )
    }
}


export default Logo


